package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

const serverURL = "http://64.227.6.226:714"
const requirementsURL = "http://64.227.6.226:714/requirements"
const evaURL = "http://64.227.6.226:714/characters/EVA-Beatrice.zip"

type requirements struct {
	Characters []string `json:"characters"`
}

func main() {
	//fmt.Print("potato12")
	//getRequirements()
	os.Mkdir("tmp", 0777)
	downloadFile("tmp/EVA.zip", evaURL)
	_, err := Unzip("tmp/EVA.zip", "base/characters/")
	if err != nil {
		panic(err)
	}
}

func getRequirements() {
	resp, err2 := http.Get(requirementsURL)
	if err2 != nil {
		panic(err2)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	resp.Body.Close()
	requirements := requirements{}
	jsonErr := json.Unmarshal(body, &requirements)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}
	fmt.Println("requirements:")
	for index, element := range requirements.Characters {
		fmt.Println(index, element)
	}
}

// func downloadCharacterFolder(character string) {
// 	// access the folder
// 	var client http.Client
// 	resp, err := client.Get(serverURL + "/characters/" + character)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer resp.Body.Close()
// 	if resp.StatusCode == http.StatusOK {
// 		bodyBytes, err := ioutil.ReadAll(resp.Body)
// 		if err != nil {
// 			log.Fatal(err)
// 		}
// 		bodyString := string(bodyBytes)
// 		fmt.Print(bodyString)
// 	}

// 	doc, _ := html.Parse(resp.Body)

// 	fmt.Print(doc.Type)
// }

// DownloadFile will download a url to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
func downloadFile(filepath string, url string) error {
	fmt.Println("Downloading the file...")
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Print("HYPER MEGA POTATO")
			panic(err)
		} else {
			return err
		}
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}
